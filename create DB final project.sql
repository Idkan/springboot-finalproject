-- -----------------------------------------------------
-- Schema todo
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `sesionFinal` ;

-- -----------------------------------------------------
-- Schema todo
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `sesionFinal` DEFAULT CHARACTER SET utf8 ;
USE `sesionFinal` ;

-- -----------------------------------------------------
-- Table `sesionFinal`.`Employee`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sesionFinal`.`Employee` ;

CREATE TABLE IF NOT EXISTS `sesionFinal`.`Employee` (
  `Employee_id` INT NOT NULL AUTO_INCREMENT,
  `ISTag` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`Employee_id`))
ENGINE = InnoDB;

ALTER TABLE `sesionFinal`.`Employee` auto_increment = 1;
-- -----------------------------------------------------
-- Table `sesionFinal`.`WorkDays`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sesionFinal`.`work_days` ;

CREATE TABLE IF NOT EXISTS `sesionFinal`.`work_days` (
  `Date_id` INT NOT NULL AUTO_INCREMENT,
  `Date_in` DATETIME NOT NULL,
  `Date_out` DATETIME NOT NULL,
  PRIMARY KEY (`Date_id`))
ENGINE = InnoDB;

ALTER TABLE `sesionFinal`.`work_days` auto_increment = 1;

-- -----------------------------------------------------
-- Table `sesionFinal`.`employee_workdays`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sesionFinal`.`employee_workdays` ;

CREATE TABLE IF NOT EXISTS `sesionFinal`.`employee_workdays` (
  `employee_id` INT NOT NULL,
  `date_id` INT NOT NULL,
  INDEX `fk_EMPLOYEE_WORKDAYS_employee` (`employee_id` ASC) VISIBLE,
  INDEX `fk_EMPLOYEE_WORKDAYS_date` (`date_id` ASC) VISIBLE,
  CONSTRAINT `fk_EMPLOYEE_WORKDAYS_employee` 
	FOREIGN KEY (`employee_id`)
    REFERENCES `sesionFinal`.`Employee`(`Employee_id`),
  CONSTRAINT `fk_EMPLOYEE_WORKDAYS_date`
	FOREIGN KEY (`date_id`)
    REFERENCES `sesionFinal`.`work_days`(`Date_id`))
ENGINE = InnoDB;

