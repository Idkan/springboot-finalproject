package com.softtek.academy.momentum.FinalProject.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;

@Entity
public class Employee {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "EmployeeId", unique = true, nullable = false)
	private int employeeId;
	
	@Column(name = "ISTag")
	private String is;
	
	@OneToMany(cascade = CascadeType.ALL ,fetch = FetchType.EAGER)
	@JoinTable(name = "employee_workdays",
	joinColumns = @JoinColumn(name = "employee_id"),
	inverseJoinColumns = @JoinColumn(name = "date_id"))
	private List<WorkDays> workDays;

	public Employee() {
		
	}
	public Employee(int employeeId, String is, ArrayList<WorkDays> workDays) {
		super();
		this.employeeId = employeeId;
		this.is = is;
		this.workDays = workDays;
	}
	public int getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	public String getIs() {
		return is;
	}
	public void setIs(String is) {
		this.is = is;
	}
	public List<WorkDays> getWorkDays() {
		return workDays;
	}
	public void setWorkDays(List<WorkDays> workDays) {
		this.workDays = workDays;
	}
	
}
