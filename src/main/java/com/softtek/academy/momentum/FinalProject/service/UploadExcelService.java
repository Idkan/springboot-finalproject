package com.softtek.academy.momentum.FinalProject.service;

import java.io.IOException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.web.multipart.MultipartFile;

public interface UploadExcelService {

	void upload(MultipartFile file) throws IOException, InvalidFormatException;

}
