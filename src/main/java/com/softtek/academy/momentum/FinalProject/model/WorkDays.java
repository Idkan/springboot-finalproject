package com.softtek.academy.momentum.FinalProject.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class WorkDays {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "DateId", unique = true, nullable = false)
	private int dateId;
	
	@Column(name = "Date_in", length = 50)
	private String dateIn;
	
	@Column(name = "Date_out", length = 50)
	private String dateOut;
			
	public WorkDays() {
		
	}
	

	public WorkDays(int dateId, String dateIn, String dateOut) {
		super();
		this.dateId = dateId;
		this.dateIn = dateIn;
		this.dateOut = dateOut;
	}


	public int getDateId() {
		return dateId;
	}

	public void setDateId(int dateId) {
		this.dateId = dateId;
	}

	public String getDateIn() {
		return dateIn;
	}

	public void setDateIn(String dateIn) {
		this.dateIn = dateIn;
	}

	public String getDateOut() {
		return dateOut;
	}

	public void setDateOut(String dateOut) {
		this.dateOut = dateOut;
	}
}
