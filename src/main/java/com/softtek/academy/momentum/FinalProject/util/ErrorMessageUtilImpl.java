package com.softtek.academy.momentum.FinalProject.util;

import java.util.LinkedHashMap;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class ErrorMessageUtilImpl implements ErrorMessageUtil {

	@Override
	public ResponseEntity<Object> errorHoursByMont(String is, String mes) {
		LinkedHashMap<String, String> response = new LinkedHashMap<>();
		HttpHeaders headers = new HttpHeaders();
		response.put("Status", "404");
		response.put("Error", "Not Found");
		response.put("Message", "Unable to find hours for this employee: " + is + " on specific month: " + mes);
		return new ResponseEntity<>(response, headers, HttpStatus.NOT_FOUND);
	}

	@Override
	public ResponseEntity<Object> errorDatesByIsAndName(String is, String mes) {
		LinkedHashMap<String, String> response = new LinkedHashMap<>();
		HttpHeaders headers = new HttpHeaders();
		response.put("Status", "404");
		response.put("Error", "Not Found");
		response.put("Message", "Unable to find workDays for this employee: " + is + " on specific month: " + mes);
		return new ResponseEntity<>(response, headers, HttpStatus.NOT_FOUND);
	}

	@Override
	public ResponseEntity<Object> errorDateFormatter(String startDate, String endDate) {
		LinkedHashMap<String, String> response = new LinkedHashMap<>();
		HttpHeaders headers = new HttpHeaders();
		response.put("Status", "500");
		response.put("Error", "Parsing Error");
		response.put("Message", "Date format from " + startDate + " and " + endDate +" must be yyyy-mm-dd");
		return new ResponseEntity<>(response, headers, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@Override
	public ResponseEntity<Object> errorStartDateAfter(String startDate, String endDate) {
		LinkedHashMap<String, String> response = new LinkedHashMap<>();
		HttpHeaders headers = new HttpHeaders();
		response.put("Status", "400");
		response.put("Error", "Bad Request");
		response.put("Message", "startDate: " + startDate + " must be before endDate: " + endDate);
		return new ResponseEntity<>(response, headers, HttpStatus.BAD_REQUEST);
	}

	@Override
	public ResponseEntity<Object> errorEmployeesByIntervalDate(String is, String startDate, String endDate) {
		LinkedHashMap<String, String> response = new LinkedHashMap<>();
		HttpHeaders headers = new HttpHeaders();
		response.put("Status", "404");
		response.put("Error", "Not Found");
		response.put("Message", "Unable to find workDays from employee: " + is + " on specific date interval: " + 
				startDate + " to " + endDate);
		return new ResponseEntity<>(response, headers, HttpStatus.NOT_FOUND);
	}

	@Override
	public ResponseEntity<Object> errorMonth(String mes) {
		LinkedHashMap<String, String> response = new LinkedHashMap<>();
		HttpHeaders headers = new HttpHeaders();
		response.put("Status", "404");
		response.put("Error", "Not Found");
		response.put("Message", "Unable to find Employees with workDays on specific month: " + mes);
		return new ResponseEntity<>(response, headers, HttpStatus.NOT_FOUND);
	}

	@Override
	public ResponseEntity<Object> errorWorkDaysByIntervalDate(String startDate, String endDate) {
		LinkedHashMap<String, String> response = new LinkedHashMap<>();
		HttpHeaders headers = new HttpHeaders();
		response.put("Status", "404");
		response.put("Error", "Not Found");
		response.put("Message", "Unable to find Employees with workDays on specific date interval: " + startDate + " to " + endDate);
		return new ResponseEntity<>(response, headers, HttpStatus.NOT_FOUND);
	}

}
