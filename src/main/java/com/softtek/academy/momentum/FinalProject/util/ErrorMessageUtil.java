package com.softtek.academy.momentum.FinalProject.util;

import org.springframework.http.ResponseEntity;

public interface ErrorMessageUtil {

	ResponseEntity<Object> errorHoursByMont(String is, String mes);

	ResponseEntity<Object> errorDatesByIsAndName(String is, String mes);

	ResponseEntity<Object> errorDateFormatter(String startDate, String endDate);

	ResponseEntity<Object> errorStartDateAfter(String startDate, String endDate);

	ResponseEntity<Object> errorEmployeesByIntervalDate(String is, String startDate, String endDate);

	ResponseEntity<Object> errorMonth(String mes);

	ResponseEntity<Object> errorWorkDaysByIntervalDate(String startDate, String endDate);

}
