package com.softtek.academy.momentum.FinalProject.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softtek.academy.momentum.FinalProject.model.Employee;
import com.softtek.academy.momentum.FinalProject.respository.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	EmployeeRepository employeeRepository;
	
	private long miliSeconds = 0;

	@Override
	public List<Employee> getAllEmployees() {
		return (List<Employee>) employeeRepository.findAll();
	}

	@Override
	public Employee getHoursByIsAndMonth(String is, String mes) {
		Employee employeeByMonth = employeeRepository.findByis(is); 
		employeeByMonth.getWorkDays().removeIf(date -> !date.getDateIn().substring(5, 7).equals(mes));

		if (employeeByMonth.getWorkDays().isEmpty()) {
			return null;
		}
		
		employeeByMonth.getWorkDays().forEach(date -> {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			sdf.setTimeZone(TimeZone.getTimeZone("America/Mexico City"));
			try {
				Date sDate = sdf.parse(date.getDateIn());
				Date eDate = sdf.parse(date.getDateOut());
				this.miliSeconds += eDate.getTime() - sDate.getTime();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		});
		if (miliSeconds != 0) {
			String hoursPerMonth = String.format("%02d:%02d:%02d",
					TimeUnit.MILLISECONDS.toHours(miliSeconds),
		            TimeUnit.MILLISECONDS.toMinutes(miliSeconds) -
		            TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(miliSeconds)),
		            TimeUnit.MILLISECONDS.toSeconds(miliSeconds) -
		            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(miliSeconds)));
			System.out.println(hoursPerMonth);
		}
		
		return employeeByMonth;
	}

	@Override
	public Employee getEmployeeByStartDateAndEndDate(String is, String startDate, String endDate) {
		Employee employeeDateInterval = employeeRepository.findByis(is);
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		sdf.setTimeZone(TimeZone.getTimeZone("America/Mexico City"));
		LocalDate sDate = LocalDate.parse(startDate).minusDays(1);
		LocalDate eDate = LocalDate.parse(endDate).plusDays(1);
		
		employeeDateInterval.getWorkDays().removeIf(date ->  !((LocalDate.parse(date.getDateIn().substring(0,10)).isAfter(sDate))
				&&(LocalDate.parse(date.getDateIn().substring(0, 10)).isBefore(eDate))));
		
		
		return employeeDateInterval;
	}

	@Override
	public List<Employee> getAllEmployeesByMonth(String mes) {
		List<Employee> employeeByMonth = employeeRepository.findAll();
		
		for (Employee employee : employeeByMonth) {
			employee.getWorkDays().removeIf(date -> !date.getDateIn().substring(5, 7).equals(mes));
		}
		
		Iterator<Employee> it = employeeByMonth.iterator();
		while (it.hasNext()) {
			Employee employee2 = (Employee) it.next();
			if (employee2.getWorkDays().isEmpty()) {
				it.remove();
			}
			
		}
		
		return employeeByMonth;
	}

	@Override
	public List<Employee> getAllByStartDateAndEndDate(String startDate, String endDate) {
		List<Employee> employeeDateInterval = employeeRepository.findAll();
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		sdf.setTimeZone(TimeZone.getTimeZone("America/Mexico City"));
		LocalDate sDate = LocalDate.parse(startDate).minusDays(1);
		LocalDate eDate = LocalDate.parse(endDate).plusDays(1);
		
		for (Employee employee : employeeDateInterval) {
			employee.getWorkDays()
				.removeIf(date ->  !((LocalDate.parse(date.getDateIn().substring(0,10)).isAfter(sDate))
								&&(LocalDate.parse(date.getDateIn().substring(0, 10)).isBefore(eDate))));
		}
		Iterator<Employee> it = employeeDateInterval.iterator();
		while (it.hasNext()) {
			Employee employee2 = (Employee) it.next();
			if (employee2.getWorkDays().isEmpty()) {
				it.remove();
			}
			
		}

		
		return employeeDateInterval;
	}

	@Override
	public String gethours(String is, String mes) {
		Employee employeeByMonth = employeeRepository.findByis(is); 
		employeeByMonth.getWorkDays().removeIf(date -> !date.getDateIn().substring(5, 7).equals(mes));
		miliSeconds = 0;
		String hoursPerMonth = "00:00:00";
		employeeByMonth.getWorkDays().forEach(date -> {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			sdf.setTimeZone(TimeZone.getTimeZone("America/Mexico City"));
			try {
				Date sDate = sdf.parse(date.getDateIn());
				Date eDate = sdf.parse(date.getDateOut());
				this.miliSeconds += eDate.getTime() - sDate.getTime();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		});
		if (miliSeconds != 0) {
			 hoursPerMonth = String.format("%02d:%02d:%02d",
					TimeUnit.MILLISECONDS.toHours(miliSeconds),
		            TimeUnit.MILLISECONDS.toMinutes(miliSeconds) -
		            TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(miliSeconds)),
		            TimeUnit.MILLISECONDS.toSeconds(miliSeconds) -
		            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(miliSeconds)));
			return hoursPerMonth;
		}
		return hoursPerMonth;
	}



}
