package com.softtek.academy.momentum.FinalProject.model;

public class EmployeeHoursByMonth {
	private int id;
	private String is;
	private String date;
	private String hours;
	
	public EmployeeHoursByMonth() {
		
	}
	
	public EmployeeHoursByMonth(int id, String is, String date, String hours) {
		super();
		this.id = id;
		this.is = is;
		this.date = date;
		this.hours = hours;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIs() {
		return is;
	}

	public void setIs(String is) {
		this.is = is;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getHours() {
		return hours;
	}

	public void setHours(String hours) {
		this.hours = hours;
	}
	
	

}
