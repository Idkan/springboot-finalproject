package com.softtek.academy.momentum.FinalProject.controller;

import java.io.IOException;
import java.util.LinkedHashMap;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.softtek.academy.momentum.FinalProject.service.UploadExcelService;

@RestController
public class UploadExcelController {
	
	@Autowired
	private final UploadExcelService uploadExcelService;
	
	public UploadExcelService getUploadExcelService() {
		return uploadExcelService;
	}
	
	public UploadExcelController(UploadExcelService uploadExcelService) {
		this.uploadExcelService = uploadExcelService;
	}
	
	@PostMapping("/uploadExcel")
	public ResponseEntity<Object> uploadExcel(@RequestParam("file") MultipartFile file) throws InvalidFormatException, IOException {
		HttpHeaders headers = new HttpHeaders();
		LinkedHashMap<String, String> response = new LinkedHashMap<>();
		if(file == null) {
			response.put("Error", "500");
			response.put("Message", "Unable to find file to upload");
			return new ResponseEntity<>(response, headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		uploadExcelService.upload(file);
		response.put("Status", "200");
		response.put("Success", "File uploaded");
		response.put("Message",  "You successfully uploaded '" + file.getOriginalFilename() + "'");
		return new ResponseEntity<>(response, headers, HttpStatus.OK);
	}


	
}
