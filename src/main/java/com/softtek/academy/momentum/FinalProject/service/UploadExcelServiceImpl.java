package com.softtek.academy.momentum.FinalProject.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.softtek.academy.momentum.FinalProject.model.Employee;
import com.softtek.academy.momentum.FinalProject.model.WorkDays;
import com.softtek.academy.momentum.FinalProject.respository.EmployeeRepository;
import com.softtek.academy.momentum.FinalProject.util.UploadExcelUtil;

@Service
public class UploadExcelServiceImpl implements UploadExcelService {

	private final String NAME = "Name";
	private final String CLOCK = "Clock-in/out";
	private final String DATE = "Date/Time";
	
	private final UploadExcelUtil uploadExcelUtil;
	private final EmployeeRepository emloyeeRepository;
	
	public UploadExcelServiceImpl(UploadExcelUtil uploadExcelUtil, EmployeeRepository emloyeeRepository) {
		this.emloyeeRepository = emloyeeRepository;
		this.uploadExcelUtil = uploadExcelUtil;
	}
	
	@Override
	public void upload(MultipartFile file) throws IOException, InvalidFormatException {
		
		Path tempDir = Files.createTempDirectory("");
		File tempFile = tempDir.resolve(file.getOriginalFilename()).toFile();
		file.transferTo(tempFile);
		
		Workbook workbook = WorkbookFactory.create(tempFile);
		Sheet sheet = workbook.getSheetAt(0);
		
		Supplier<Stream<Row>> rowStreamSupplier = uploadExcelUtil.getRowStreamSupplier(sheet);
		
		Row headerRow = rowStreamSupplier.get().findFirst().get();
		
		List<String> headerCells = uploadExcelUtil.getRowStream(headerRow)
				.filter(cell -> 
					cell.getColumnIndex() == 2 || 
					cell.getColumnIndex() == 4 || 
					cell.getColumnIndex() == 6)
				.map(Cell::getStringCellValue)
				.collect(Collectors.toList());
		
		int columCount = headerCells.size();
		List<Map<String, String>> listMap = rowStreamSupplier.get()
				.skip(1)
				.map(row -> {
					List<String> cellList = uploadExcelUtil.getRowStream(row)
							.filter(columnValue -> 
								columnValue.getColumnIndex() == 2 || 
								columnValue.getColumnIndex() == 4 || 
								columnValue.getColumnIndex() == 6)
							.map(Cell::getStringCellValue)
							.collect(Collectors.toList());
			
			return uploadExcelUtil.cellIteratorSupplier(columCount)
					.get()
					.collect(Collectors.toMap(
							index -> headerCells.get(index), 
							index -> cellList.get(index)));
		})
		.collect(Collectors.toList());

		// Sort listMap A to Z
		Collections.sort(listMap, new Comparator<Map<String, String>>() {
			public int compare(final Map<String, String> map1, final Map<String, String> map2) {
				return map1.get("Name").compareTo(map2.get("Name"));
			}
		});
														
		List<JsonValue> list = filterList(listMap).getJsonArray("employeeData");
		saveIntoDB(list);
        
	}
	
	public JsonObject filterList(List<Map<String, String>> listMap) {
		
		JsonObjectBuilder filtredData = Json.createObjectBuilder();
		
		Map<String, Object> config = new HashMap<String, Object>();
        config.put("javax.json.stream.JsonGenerator.prettyPrinting", Boolean.valueOf(true));
		JsonBuilderFactory factory = Json.createBuilderFactory(config);
		
		JsonArrayBuilder dateTempFactory = factory.createArrayBuilder();
		JsonArrayBuilder userTempFactory = factory.createArrayBuilder();

		Map<String,String> dummy  = new HashMap<String, String>();
		dummy.put("", "");
		dummy.put("", "");
		dummy.put("", "");
		listMap.add(dummy);
		
		int employeeIdCount = 1;
		int dateIdCount = 1;

		for(int i = 0; i < listMap.size() - 1; i++){
			if (listMap.get(i).containsKey(CLOCK) && listMap.get(i).containsKey(DATE) && listMap.get(i).containsKey(NAME)){
				String currentName = listMap.get(i).get(NAME);
				String nextName = listMap.get(i+1).get(NAME);	
				if (currentName.equals(nextName)){
					String currentDate = listMap.get(i).get(DATE).substring(0,10);
					String nextDate = listMap.get(i+1).get(DATE).substring(0,10);
					if (currentDate.equals(nextDate)){
						String currentClock = listMap.get(i).get(CLOCK);
						String nextClock = listMap.get(i+1).get(CLOCK);	
		    			if(currentClock.contains("In") && nextClock.contains("Out")){
		    				int currentTime = Integer.parseInt(listMap.get(i).get(DATE).substring(11,13));
		    				int nextTime = Integer.parseInt(listMap.get(i+1).get(DATE).substring(11,13));
		    				if (nextTime > currentTime) {
//		    					System.out.println("Empleado registro entrada y salida correctamente.");
								dateTempFactory.add(factory.createObjectBuilder()
									.add("dateId", dateIdCount)
									.add("C/InDate", listMap.get(i).get(DATE))
									.add("C/OutDate", listMap.get(i+1).get(DATE)));
									dateIdCount++;
									i++;
									if (i < listMap.size()-1) {
										String tempNexName = listMap.get(i+1).get(NAME);
										if(currentName != tempNexName) {
											userTempFactory.add(factory.createObjectBuilder()
												.addAll(factory.createObjectBuilder()
												.add("employeeId", employeeIdCount)
												.add("IS", currentName)
												.add("workDayData", dateTempFactory)));
											dateIdCount = 1;
											employeeIdCount++;
//											System.out.println("registro 2");
										}
									}
							}
						} else {
							int currentTime = Integer.parseInt(listMap.get(i).get(DATE).substring(11,13));
		    				int nextTime = Integer.parseInt(listMap.get(i+1).get(DATE).substring(11,13));
		    				if (nextTime > currentTime) {
								dateTempFactory.add(factory.createObjectBuilder()
									.add("dateId", dateIdCount)
									.add("C/InDate", listMap.get(i).get(DATE))
									.add("C/OutDate", listMap.get(i+1).get(DATE)));
									dateIdCount++;
									i++;
//									System.out.println(" cin cout Arreglado");
									if (i < listMap.size()-1) {
										String tempNexName = listMap.get(i+1).get(NAME);
										if(currentName != tempNexName) {
											userTempFactory.add(factory.createObjectBuilder()
												.addAll(factory.createObjectBuilder()
												.add("employeeId", employeeIdCount)
												.add("IS", currentName)
												.add("workDayData", dateTempFactory)));
											dateIdCount = 1;
											employeeIdCount++;
//											System.out.println("registro 2");
										}
									}
									
							}
//							System.out.println("Información de Biometrico erronea");
						}
					} else {
//						System.out.println("No Registró Salida");
					}
				} else {
					userTempFactory.add(factory.createObjectBuilder()
							.addAll(factory.createObjectBuilder()
							.add("employeeId", employeeIdCount)
							.add("IS", currentName)
							.add("workDayData", dateTempFactory)));
//					System.out.println("Otro Empleado");
					dateIdCount = 1;
					employeeIdCount++;
				}
			}
		}
		
		JsonObject jsonFiltred = filtredData.add("employeeData", userTempFactory).build();
		
		return jsonFiltred;
	}
	
	public void saveIntoDB(List<JsonValue> list) {
			for(int i = 0; i < list.size(); i ++ ) {
        	
//        	System.out.println(list.get(i).asJsonObject().get("employeeId"));
//        	System.out.println(list.get(i).asJsonObject().get("IS"));
        	
        	List<JsonValue> list2 = list.get(i).asJsonObject().getJsonArray("workDayData");
        	
        	List<WorkDays> workDays = new ArrayList<WorkDays>();
			Employee employee = new Employee();
			employee.setIs(list.get(i).asJsonObject().get("IS").toString().replaceAll("\"",""));
			
			for (int j = 0; j < list2.size(); j++) {
				
				WorkDays workDay = new WorkDays();
				
//				System.out.println(list2.get(j).asJsonObject().get("dateId"));
//				System.out.println(list2.get(j).asJsonObject().get("C/InDate"));
//				System.out.println(list2.get(j).asJsonObject().get("C/OutDate"));
				
				workDay.setDateIn(list2.get(j).asJsonObject().get("C/InDate").toString().replaceAll("\"",""));
				workDay.setDateOut(list2.get(j).asJsonObject().get("C/OutDate").toString().replaceAll("\"",""));
				workDays.add(workDay);
				
			}
			employee.setWorkDays(workDays);
			emloyeeRepository.save(employee);
        }
	}

}
