package com.softtek.academy.momentum.FinalProject.util;

import java.util.function.Supplier;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.apache.poi.ss.usermodel.Row;
import org.springframework.stereotype.Component;

@Component
public class UploadExcelUtilImpl implements UploadExcelUtil {

	@Override
	public Supplier<Stream<Row>> getRowStreamSupplier(Iterable<Row> rows) {
		return () -> getRowStream(rows);
	}

	@Override
	public <T> Stream<T> getRowStream(Iterable<T> iterable) {
		return StreamSupport.stream(iterable.spliterator(), false);
	}

	@Override
	public Supplier<Stream<Integer>> cellIteratorSupplier(int end) {
		return () -> numberStream(end);
	}

	@Override
	public Stream<Integer> numberStream(int end) {
		return IntStream.range(0, end).boxed();
	}

}
