package com.softtek.academy.momentum.FinalProject.util;

import java.util.function.Supplier;
import java.util.stream.Stream;

import org.apache.poi.ss.usermodel.Row;

public interface UploadExcelUtil {
	
	Supplier<Stream<Row>> getRowStreamSupplier(Iterable<Row> rows);
	<T> Stream<T> getRowStream(Iterable<T> iterable);
	Supplier<Stream<Integer>> cellIteratorSupplier(int end);
	Stream<Integer> numberStream(int end);

}
