package com.softtek.academy.momentum.FinalProject.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.ResolverStyle;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;

import com.softtek.academy.momentum.FinalProject.error.EmployeeNotFoundException;
import com.softtek.academy.momentum.FinalProject.model.Employee;
import com.softtek.academy.momentum.FinalProject.model.EmployeeHoursByMonth;
import com.softtek.academy.momentum.FinalProject.service.EmployeeService;
import com.softtek.academy.momentum.FinalProject.service.UploadExcelService;

@Controller 
public class ViewController {
	
	@Autowired
	private final UploadExcelService uploadExcelService;
	
	@Autowired
	private final EmployeeService employeeService;
	
	public EmployeeService getEmployeeService() {
		return employeeService;
	}

	public UploadExcelService getUploadExcelService() {
		return uploadExcelService;
	}
	
	public ViewController(UploadExcelService uploadExcelService, EmployeeService employeeService) {
		this.uploadExcelService = uploadExcelService;
		this.employeeService = employeeService;
	}
	
	@GetMapping(value = "/")
	public String indexPage() {
		return "views/index";
	}
	
	@GetMapping(value = "/uploadExcelFile")
	public String uploadExcelFileView() {
		return "views/uploadExcel";
	}
	@PostMapping(value = "/postuploadExcelFile")
	public String uploadExcelFile(MultipartFile file, Model model) {
		boolean messageFlag = false;
		try {
			uploadExcelService.upload(file);
			messageFlag = true;
		} catch (InvalidFormatException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			messageFlag = false;
		}
		if(messageFlag) {
			model.addAttribute("successMessage", "File Upload Successfully!");
		} else {
			model.addAttribute("errorMessage", "Unable to upload file, Pleas try again!");
		}
		return "views/uploadExcel";
	}
	
	@GetMapping(value = "/momentumsData")
	public String getAllMomentumData(Model model) {
		List<Employee> listEmployee = employeeService.getAllEmployees();
		model.addAttribute("employees", listEmployee);
		return "views/momentumsData";
	}
	
	@GetMapping(value = "/hoursByMonthForm")
	public String getISHoursbyMonthView() {
		return "views/hoursByMonthForm";
	}
	
	@GetMapping(value = "/gethoursByMonthForm")
	public String getISHoursbyMonth(Model model, String is, String month) {
		String mes = month.substring(5,7);
		String hours = "";
		if(Integer.parseInt(mes) < 1 || Integer.parseInt(mes) > 12) {
			throw new EmployeeNotFoundException("We can't search on month: " + mes + " only accept values between 01-12");
		}
		try {
			hours = employeeService.gethours(is, mes);
			if(hours.equals("00:00:00")){
				throw new EmployeeNotFoundException("Unable to find hours for this employee: " + is + " on specific month: " + mes);
			}
		}catch (NullPointerException   e) {
			throw new EmployeeNotFoundException("Employee IS not found: " + is);
		}
	
		EmployeeHoursByMonth employeeHoursByMonth = new EmployeeHoursByMonth();
		Employee employeeTemp = employeeService.getHoursByIsAndMonth(is, mes);
		employeeHoursByMonth.setId(employeeTemp.getEmployeeId());
		employeeHoursByMonth.setIs(employeeTemp.getIs());
		employeeHoursByMonth.setDate(employeeTemp.getWorkDays().get(0).getDateIn().substring(0,7));
		employeeHoursByMonth.setHours(employeeService.gethours(is, mes));
		model.addAttribute("employeeHours", employeeHoursByMonth);
		return "views/hoursByMonthForm";
	}
	
	
	@GetMapping(value = "/intervalTimeForm")
	public String getMomentumDataByIntervalTimeView() {
		return "views/intervalTimeForm";
	}
	@PostMapping(value = "/postintervalTimeForm")
	public String getMomentumDataByIntervalTime(Model model, String is, String startDate, String endDate) {
		Employee employeeByIntervalDate = new Employee();
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		
		try {
			DateTimeFormatter.ofPattern("yyyy-MM-dd").withResolverStyle(ResolverStyle.STRICT).parse(startDate);
		}catch (RuntimeException  e) {
			throw new EmployeeNotFoundException("Date format from " + startDate + " and " + endDate +" must be yyyy-mm-dd");
		}
		if (LocalDate.parse(startDate,formatter).isAfter(LocalDate.parse(endDate,formatter))) {
			throw new EmployeeNotFoundException("startDate: " + startDate + " must be before endDate: " + endDate);
		} 
		try {
			employeeByIntervalDate = employeeService.getEmployeeByStartDateAndEndDate(is, startDate, endDate);
			if (employeeByIntervalDate.getWorkDays().isEmpty()) {
				throw new EmployeeNotFoundException("Unable to find workDays from employee: " + is + " on specific date interval: " + 
						startDate + " to " + endDate);
			}
		} catch (NullPointerException e) {
			throw new EmployeeNotFoundException("Employee IS not found: " + is);
		}
		model.addAttribute("employeeIntervalTime",employeeByIntervalDate);
		return "views/intervalTimeForm";
	}
	
	@GetMapping(value = "/dataByMonthForm")
	public String getAllMomentumDataByMonthView() {
		return "views/dataByMonthForm";
	}
	@GetMapping(value = "/getdataByMonthForm")
	public String getAllMomentumDataByMonth(Model model, String month) {
		String mes = month.substring(5,7);
		if(Integer.parseInt(mes) < 1 || Integer.parseInt(mes) > 12) {
			throw new EmployeeNotFoundException("We can't search on month: " + mes + " only accept values between 01-12");
		}
		List<Employee> employeesByMonth = new ArrayList<>();
		employeesByMonth = employeeService.getAllEmployeesByMonth(mes);
		if (employeesByMonth.isEmpty()) {
			model.addAttribute("errorMonth",("Unable to find Employees with workDays on specific month: " + mes));
		}
		model.addAttribute("dataByMonth",employeesByMonth);
		return "views/dataByMonthForm";
	}
	
	@GetMapping(value = "/dataByIntervalTimeForm")
	public String getAllMomentumDataByIntervalTimeView() {
		return "views/dataByIntervalTimeForm";
	}
	@PostMapping(value = "/postdataByIntervalTimeForm")
	public String getAllMomentumDataByIntervalTime(Model model, String startDate, String endDate) {
		List<Employee> employeesByIntervalTime = new ArrayList<>();		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		
		try {
			DateTimeFormatter.ofPattern("yyyy-MM-dd").withResolverStyle(ResolverStyle.STRICT).parse(startDate);
		}catch (RuntimeException  e) {
			throw new EmployeeNotFoundException("Date format from " + startDate + " and " + endDate +" must be yyyy-mm-dd");
		}
		if (LocalDate.parse(startDate,formatter).isAfter(LocalDate.parse(endDate,formatter))) {
			throw new EmployeeNotFoundException("startDate: " + startDate + " must be before endDate: " + endDate);	
		} 
		employeesByIntervalTime = employeeService.getAllByStartDateAndEndDate(startDate, endDate);
		if(employeesByIntervalTime.isEmpty()) {
			model.addAttribute("errorInterval",("Unable to find Employees with workDays on specific date interval: " + startDate + " to " + endDate));
		}

		model.addAttribute("employeesByIntervalTime", employeesByIntervalTime);
		return "views/dataByIntervalTimeForm";
	}
}
