package com.softtek.academy.momentum.FinalProject.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.ResolverStyle;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.softtek.academy.momentum.FinalProject.error.EmployeeNotFoundException;
import com.softtek.academy.momentum.FinalProject.model.Employee;
import com.softtek.academy.momentum.FinalProject.model.EmployeeHoursByMonth;
import com.softtek.academy.momentum.FinalProject.service.EmployeeService;
import com.softtek.academy.momentum.FinalProject.util.ErrorMessageUtil;

@RestController
public class EmployeeController {

	@Autowired
	private final EmployeeService employeeService;
	@Autowired
	private final ErrorMessageUtil errorMessage;

	public EmployeeService getEmployeeService() {
		return employeeService;
	}
	
	public ErrorMessageUtil getErrorMessageUtil() {
		return errorMessage;
	}
	
	public EmployeeController(EmployeeService employeeService, ErrorMessageUtil errorMessage) {
		this.employeeService = employeeService;
		this.errorMessage = errorMessage;
	}
	
	@GetMapping("/api/v1/getAllEmployees")
	public List<Employee> getAllEmployees() {
		return employeeService.getAllEmployees();
	}
	
	//1. Get Hours By Is and Month 
	@GetMapping("/api/v1/users/{is}/{mes}")
	public ResponseEntity<Object> getHoursByIsAndMonth(@RequestParam String is, @RequestParam String mes) throws IOException {
		HttpHeaders headers = new HttpHeaders();
		
		EmployeeHoursByMonth employeeHoursByMonth = new EmployeeHoursByMonth();
		String hours = "";
		
		if(Integer.parseInt(mes) < 1 || Integer.parseInt(mes) > 12) {
			throw new EmployeeNotFoundException("We can't search on month: " + mes + " only accept values between 01-12");
		}
		try {
			hours = employeeService.gethours(is, mes);
			if(hours.equals("00:00:00")){
				return errorMessage.errorHoursByMont(is,mes);
			}	
		} catch (NullPointerException   e) {
			throw new EmployeeNotFoundException("Employee IS not found: " + is);
		}
		Employee employee = employeeService.getHoursByIsAndMonth(is, mes);

		employeeHoursByMonth.setId(employee.getEmployeeId());
		employeeHoursByMonth.setIs(employee.getIs());
		employeeHoursByMonth.setDate(employee.getWorkDays().get(0).getDateIn().substring(0,7));
		employeeHoursByMonth.setHours(employeeService.gethours(is, mes));
		
		return new ResponseEntity<>(employeeHoursByMonth, headers, HttpStatus.OK);
	}
	
	//1.2 Get Dates By Is and Month 
	@GetMapping("/api/v2/users/{is}/{mes}")
	public ResponseEntity<Object> getDatesByIsAndMonth(@RequestParam String is, @RequestParam String mes) throws IOException{
		HttpHeaders headers = new HttpHeaders();
		
		Employee employeeMonth = new Employee();
		String hours = "";
		
		if(Integer.parseInt(mes) < 1 || Integer.parseInt(mes) > 12) {
			throw new EmployeeNotFoundException("We can't search on month: " + mes + " only accept values between 01-12");
		} 
		try {
			hours = employeeService.gethours(is, mes);
			if(hours.equals("00:00:00")) {
				return errorMessage.errorDatesByIsAndName(is, mes);
			}
		} catch (Exception e) {
			throw new EmployeeNotFoundException("Employee IS not found: " + is);		
		}
		employeeMonth = employeeService.getHoursByIsAndMonth(is, mes);
		
		return new ResponseEntity<>(employeeMonth, headers, HttpStatus.OK);
	}

	//2. GetEmployee by StartDate and EndDate
	@PostMapping("/api/v1/users")
	public ResponseEntity<Object> getEmployeeByStartDateAndEndDate(@RequestParam String is, 
			@RequestParam String startDate, @RequestParam String endDate) throws IOException{
		HttpHeaders headers = new HttpHeaders();
		
		Employee employeeByIntervalDate = new Employee();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		
		try {
			DateTimeFormatter.ofPattern("yyyy-MM-dd").withResolverStyle(ResolverStyle.STRICT).parse(startDate);
		}catch (RuntimeException  e) {
			return errorMessage.errorDateFormatter(startDate, endDate);
		}
		if (LocalDate.parse(startDate,formatter).isAfter(LocalDate.parse(endDate,formatter))) {
			return errorMessage.errorStartDateAfter(startDate, endDate);
		} 
		try {
			employeeByIntervalDate = employeeService.getEmployeeByStartDateAndEndDate(is, startDate, endDate);
			if (employeeByIntervalDate.getWorkDays().isEmpty()) {
				return errorMessage.errorEmployeesByIntervalDate(is, startDate, endDate);
			}
		} catch (NullPointerException e) {
			throw new EmployeeNotFoundException("Employee IS not found: " + is);
		}
		return new ResponseEntity<>(employeeByIntervalDate, headers, HttpStatus.OK);
	}
	
	//3. Get all emplyees by month
	@GetMapping("/api/v1/period/{mes}")
	public ResponseEntity<Object> getAllEmployeesByMonth(@RequestParam String mes) throws IOException{
		HttpHeaders headers = new HttpHeaders();
		
		List<Employee> employeesByMonth = new ArrayList<>();
		
		if (Integer.parseInt(mes) < 1 || Integer.parseInt(mes) > 12) {
			throw new EmployeeNotFoundException("We can't search on month: " + mes + " only accept values between 01-12");
		}
		try {
			employeesByMonth = employeeService.getAllEmployeesByMonth(mes);
			if(employeesByMonth.isEmpty()) {
				return errorMessage.errorMonth(mes);
			}
		} catch (NullPointerException e) {
			e.getMessage();
		}
		return new ResponseEntity<>(employeesByMonth, headers, HttpStatus.OK);	
	}
	
	//4. Get all emplyees by StartDate and EndDate
	@PostMapping("/api/v1/period/")
	public ResponseEntity<Object>  getAllByStartDateAndEndDate(@RequestParam String startDate, 
			@RequestParam String endDate) throws IOException{
		HttpHeaders headers = new HttpHeaders();
		
		List<Employee> employeesByIntervalTime = new ArrayList<>();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		
		try {
			DateTimeFormatter.ofPattern("yyyy-MM-dd").withResolverStyle(ResolverStyle.STRICT).parse(startDate);
		}catch (RuntimeException  e) {
			return errorMessage.errorDateFormatter(startDate, endDate);
		}
		
		if (LocalDate.parse(startDate,formatter).isAfter(LocalDate.parse(endDate,formatter))) {
			return errorMessage.errorStartDateAfter(startDate, endDate);
		} 
		try {
			employeesByIntervalTime = employeeService.getAllByStartDateAndEndDate(startDate, endDate);
			if(employeesByIntervalTime.isEmpty()) {
				return errorMessage.errorWorkDaysByIntervalDate(startDate, endDate);
			}
		} catch (Exception e) {
			e.getMessage();

		}
		return new ResponseEntity<>(employeesByIntervalTime, headers, HttpStatus.OK);	
	}
	
}
