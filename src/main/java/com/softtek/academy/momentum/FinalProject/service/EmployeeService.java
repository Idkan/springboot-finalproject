package com.softtek.academy.momentum.FinalProject.service;

import java.util.List;

import com.softtek.academy.momentum.FinalProject.model.Employee;

public interface EmployeeService {

	List<Employee> getAllEmployees();

	Employee getHoursByIsAndMonth(String is, String mes);

	Employee getEmployeeByStartDateAndEndDate(String is, String startDate, String endDate);

	List<Employee> getAllEmployeesByMonth(String mes);

	List<Employee> getAllByStartDateAndEndDate(String startDate, String endDate);

	String gethours(String is, String mes);

}
