package com.softtek.academy.momentum.FinalProject.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.softtek.academy.momentum.FinalProject.model.Employee;
import com.softtek.academy.momentum.FinalProject.model.EmployeeHoursByMonth;
import com.softtek.academy.momentum.FinalProject.model.WorkDays;
import com.softtek.academy.momentum.FinalProject.respository.EmployeeRepository;

@SpringBootTest
public class EmployeeServiceTest {
	
	@Autowired
	EmployeeService employeeService;
	
	@Autowired
	EmployeeRepository employeeRepository;
	
	@Test
	public void TestGethours() {
		// Setup
		String is = "JASA5";
		String month = "02";
		String expectedHoursPerMonth = "14:46:26";
		
    	WorkDays workDay = new WorkDays(1,"2019-02-02 06:43:41","2019-02-02 14:06:54");
    	WorkDays workDay2 = new WorkDays(2,"2019-02-03 06:43:41","2019-02-03 14:06:54");
    	ArrayList<WorkDays> listWorkDays = new ArrayList<>();
    	listWorkDays.add(workDay);
    	listWorkDays.add(workDay2);
    	Employee employee = new Employee(1,"JASA5",listWorkDays);
    	
    	// Execute
    	employeeRepository.saveAndFlush(employee);
    	String actualHoursPerMonth = employeeService.gethours(is, month);
    	
    	// Validate 
    	assertNotNull(actualHoursPerMonth);
    	assertEquals(expectedHoursPerMonth, actualHoursPerMonth);
		
		
	}
	
	@Test
	public void TestGetHoursByIsAndMonth() {
    	// Setup
		String mes = "01";
		String expectedIs = "JASA5";
		String expectedDate = "2019-01";
		String expectedHours = "14:46:26";
		
    	WorkDays workDay = new WorkDays(1,"2019-01-02 06:43:41","2019-01-02 14:06:54");
    	WorkDays workDay2 = new WorkDays(2,"2019-01-03 06:43:41","2019-01-03 14:06:54");
    	ArrayList<WorkDays> listWorkDays = new ArrayList<>();
    	listWorkDays.add(workDay);
    	listWorkDays.add(workDay2);
    	Employee expectedEmployee = new Employee(1,"JASA5",listWorkDays);
    	EmployeeHoursByMonth employeeHoursByMonth = new EmployeeHoursByMonth();
    	
    	// Execute
    	employeeRepository.saveAndFlush(expectedEmployee);
    	Employee actualEmployee = employeeService.getHoursByIsAndMonth(expectedIs, mes);
    	employeeHoursByMonth.setId(actualEmployee.getEmployeeId());
		employeeHoursByMonth.setIs(expectedEmployee.getIs());
		employeeHoursByMonth.setDate(expectedEmployee.getWorkDays().get(0).getDateIn().substring(0,7));
		employeeHoursByMonth.setHours(employeeService.gethours(expectedIs, mes));
		
    	// Validate
    	assertNotNull(employeeHoursByMonth);
    	assertEquals(expectedEmployee.getIs(), employeeHoursByMonth.getIs());
    	assertEquals(expectedDate, employeeHoursByMonth.getDate());
    	assertEquals(expectedHours, employeeHoursByMonth.getHours());
	}
	
	@Test
	public void TestGetEmployeeByStartDateAndEndDate() {
    	// Setup
		String is = "JASA5";
		String startDate = "2019-02-03";
		String endDate = "2019-02-05";
		
    	WorkDays workDay = new WorkDays(1,"2019-01-02 06:43:41","2019-01-02 14:06:54");
    	WorkDays workDay2 = new WorkDays(2,"2019-02-03 06:43:41","2019-02-03 14:06:54");
    	WorkDays workDay3 = new WorkDays(3,"2019-02-05 06:43:41","2019-02-05 14:06:54");
    	ArrayList<WorkDays> listWorkDays = new ArrayList<>();
    	listWorkDays.add(workDay);
    	listWorkDays.add(workDay2);
    	listWorkDays.add(workDay3);
    	Employee expectedEmployee = new Employee(1,"JASA5",listWorkDays);
    	
    	// Execute
    	employeeRepository.saveAndFlush(expectedEmployee);
    	Employee actualEmployee = employeeService.getEmployeeByStartDateAndEndDate(is, startDate, endDate);
    	
    	// Validate
    	assertNotNull(actualEmployee);
    	assertThat(actualEmployee.getIs()).isEqualTo(expectedEmployee.getIs());
    	assertThat(actualEmployee.getWorkDays().get(0).getDateIn())
    		.isEqualTo(expectedEmployee.getWorkDays().get(1).getDateIn());
	}
	
	@Test
	public void TestGetAllEmployeesByMonth() {
		// Setup
    	WorkDays workDay = new WorkDays(1,"2019-01-02 06:43:41","2019-01-02 14:06:54");
    	WorkDays workDay2 = new WorkDays(2,"2019-02-03 06:43:41","2019-02-03 14:06:54");
    	WorkDays workDay3 = new WorkDays(3,"2019-02-05 06:43:41","2019-02-05 14:06:54");
    	ArrayList<WorkDays> listWorkDays = new ArrayList<>();
    	listWorkDays.add(workDay);
    	listWorkDays.add(workDay2);
    	listWorkDays.add(workDay3);
    	Employee employee1 = new Employee(1,"JASA5",listWorkDays);
    	Employee employee2 = new Employee(2,"UWU1",listWorkDays);
    	List<Employee> expectedList = new ArrayList<>();
    	expectedList.add(employee1);
    	expectedList.add(employee2);
    	
    	// Execute
    	employeeRepository.saveAndFlush(employee1);
    	employeeRepository.saveAndFlush(employee2);
    	List<Employee> actualList = employeeService.getAllEmployeesByMonth("02");
    	
    	// Validate 
    	assertNotNull(actualList);
    	assertThat(actualList.get(0).getIs()).isEqualTo(expectedList.get(0).getIs());
	}
	

}
