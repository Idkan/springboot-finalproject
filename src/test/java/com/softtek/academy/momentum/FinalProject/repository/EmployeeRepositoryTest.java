package com.softtek.academy.momentum.FinalProject.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import com.softtek.academy.momentum.FinalProject.model.Employee;
import com.softtek.academy.momentum.FinalProject.model.WorkDays;
import com.softtek.academy.momentum.FinalProject.respository.EmployeeRepository;

@SpringBootTest
public class EmployeeRepositoryTest {
	
    @Autowired
    private EmployeeRepository employeeRepository;
    
    @Test
    public void findByIsTest() {
    	
    	// Setup
    	WorkDays workDay = new WorkDays(1,"2019-01-02 06:43:41","2019-01-02 14:06:54");
    	ArrayList<WorkDays> listWorkDays = new ArrayList<>();
    	listWorkDays.add(workDay);
    	Employee employee = new Employee(1,"JASA5",listWorkDays);
    	
    	// Execute
    	employeeRepository.saveAndFlush(employee);
    	Employee employeeFind = employeeRepository.findByis(employee.getIs());
    	
    	// Validate
    	assertNotNull(employeeFind);
    	assertThat(employeeFind.getIs()).isEqualTo(employee.getIs());

    }
}
