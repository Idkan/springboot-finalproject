package com.softtek.academy.momentum.FinalProject;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.softtek.academy.momentum.FinalProject.controller.UploadExcelController;

@RunWith(SpringRunner.class)
@SpringBootTest
class FinalProjectApplicationTests {
	
	@Autowired
	private UploadExcelController uploadExcelController;

	@Test
	void contextLoads() {
		assertThat(uploadExcelController).isNotNull();
	}

}
